This is a repository with sample code for my blog post about ES6 generators and async/await.

[The blog post is here](http://blog.davidjs.com/2016/08/es6-generators-and-asyncawait/)


Feel free to fork and experiment, but please provide a link to the original artical

## How to run the code

- download :)
- install grunt-cli with global flag
- run npm install
- run grunt
- run `node es6\file-name.js` to see what it does
- TODO: maybe??
- grunt task will compile code inside es6 folder into es5 folder which is runnable in browser (provided it has the regenerator runtime available)