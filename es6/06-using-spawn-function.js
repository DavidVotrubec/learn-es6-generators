"use strict";

var spawn = require('./04-spawn-function');

spawn(function*(){
  const articleId = '1234abcd';
   
  try {
    const article = yield loadArticle(articleId);
    // When article is loaded, load comments
    const comments = yield loadComments(articleId);
    // When comments are loaded, load article's author
    const author = yield loadAuthor(article.authorId);
     
    // in real life you would probably run all 3 request in parallel
  }
  catch (err) {
    console.log('error loading', err);
    alert('Error loading article');
  }
});

/////////////////////////////////////////////////////////////////
// helper functions
/////////////////////////////////////////////////////////////////
function getTimeoutPromise(timeout, resultValue, shouldReject) {
  const promise = new Promise(function(resolve, reject) {
      setTimeout(function() {
          if (shouldReject) {
            reject();
          }
          else {
            resolve(resultValue);
          }          
      }, timeout);
  });

  return promise;
}  

function loadArticle(articleId) {
  const promise = getTimeoutPromise(2000, {authorId: articleId + '_author'});

  promise.then((data) => {
    console.log("successfully resolving articleId", articleId, data);
  }); 

  return promise;
}

function loadComments(articleId) {
  const promise = getTimeoutPromise(2000, {});

  promise.then((data) => {
    console.log("successfully resolving comments for articleId", articleId, data);
  }); 

  return promise;
}

function loadAuthor(articleId) {
  const promise = getTimeoutPromise(2000);

  promise.then(() => {
    console.log("successfully resolving author of articleId", articleId);
    resolve();
  }); 

  return promise;
}