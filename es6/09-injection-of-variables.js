"use strict";

function* generator(){
    // run until first yield
    const x = 1;
    const y = yield x;

    // when iterator is resumed, then 'y' will have value supplied as argument to next()
    console.log(`y is now ${y}`);

    // yield out value of x + y 
    // and wait for new value to be injected into variable z
    const z = yield (x + y);
    console.log(`z is ${z}`);

    yield (x + y + z);
}

(function(){
    const iterator = generator();
    //
    // 1) Start the iteration
    //
    // run until first yield
    console.log('iterator started');
    var status = iterator.next();

    // outputs {value: 1, done: false}
    console.log('status is', status);

    //
    // 2nd step
    //
    // send in value for 'y'
    console.log('sending in value of y = 10');
    status = iterator.next(10);

    // outputs {value: 11, done: false}
    console.log('status is', status); 

    //
    // 3rd step
    //
    // send in value for 'z'
    // this will console.log the message 'y is now 11'
    console.log('sending in value of z = 100');
    status = iterator.next(100);

    // outputs {value: 111, done: false}
    console.log('status is', status);

    //
    // 4th step
    //
    // we are now at the 'yield (x + y + z)'
    // there is no assignment to variable,
    // so the injected value will be simply ignored  
    status = iterator.next(1000);

    // we are done now
    // outputs {value: undefined, done: false}
    console.log('status is', status); 

})();