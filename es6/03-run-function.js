"use strict";

/**
 * The runner function repeatedly calls the supplied generator function
 * until all yields are exhausted.
 * 
 * The function passed to run() can be a thunked function. See thunkify.js
 * 
 * IT DOES NOT WORK WITH PROMISES
 * because when continuable.value is a promise object, it can not be called as a function
 * For production use it is better that you use the co() utility - see https://github.com/tj/co/
 */
module.exports = function run(/*iterator fn*/ generatorFn) {
  var iterator = generatorFn(); // [1]
  next(); // [2]

  function next (err, value) { // [3]
    if (err) {
      return iterator.throw(err);
    }

    var continuable = iterator.next(value); 

    if (continuable.done) return; // [4]

    var callbackFn = continuable.value; // [5]
    callbackFn(next);
  }
};