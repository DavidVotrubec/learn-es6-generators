"use strict";

/**
 * Thunkify take single function as its argument [1]
 * and returns a parameters-less function [2]
 * which when called returns another function [3]
 * The thunked function is very useful when used with generators and the run function (see 03-run-function.js)
 * 
 * --------------------------------------------------------------------
 * Example usage
 * var fs = require('fs')
 * var readFile = thunkify(fs.readFile) // [1]
 * var readAsyncJs = readFile('./myFile.js') // [2]
 * readMyFileJs(function (err, fileData) { ... }) // [3]
 * --------------------------------------------------------------------
 * 
 * inspired by https://strongloop.com/strongblog/how-to-generators-node-js-yield-use-cases/
 */
module.exports = function thunkify (nodefn) { // [1]
  return function () { // [2]
    var args = Array.prototype.slice.call(arguments);
    return function (cb) { // [3]
      args.push(cb); // add cb to the argument array
      nodefn.apply(this, args);
    }
  }
};
