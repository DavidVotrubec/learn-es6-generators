// "use strict";

// /**
//  * Create a `spawn` function to deal with promises
//  * Inspired by https://gist.github.com/dtothefp/a0a6a5c2e3dae8bc0d0f
//  */
// module.exports = function spawn(generatorFn) {
//   const iterator = generatorFn(); //instantiate the generator and return the iterator object

//   // create helper function which will call itself repeatedly
//   function _co(method, arg) {
//     let result;

//     try {
//       // retrieve the promise if `arg` is undefined (eg: returned by the http request) 
//       // if `arg` is defined it will be the data from the promise
//       // and will be "injected" into `yield` and caught in a variable      
//       result = iterator[method](arg); //method is either 'next' or 'throw'
//     } catch(err) {
//       return Promise.reject(err);
//     }

//     if (result.done) {
//       if (method === 'throw') {
//         return arg;
//       } else {
//         return result.value;
//       }
//     } else {
//         // Using the static Promise.resolve() method
//         // it will convert the result.value into a Promise object
//         // making it thenable.
//         // If there was an exception it will rethrown via _co('throw', err)

//         return Promise.resolve(result.value)
//           .then((val) => {
//             return _co('next', val);
//           }, (err) => {
//             return _co('throw', err);
//           });
//       }
//   }

//   // start the process by calling `.next` on the iterator instance
//   return _co('next'); 
// };

// simpler version of spawn() from https://tc39.github.io/ecmascript-asyncawait/#desugaring
// much easier to read and reason about 

// For production use it is better that you use the co() utility - see https://github.com/tj/co/
module.exports = function spawn(genF, self) {
    return new Promise(function(resolve, reject) {
        var gen = genF.call(self);

        function step(nextFn) {
            var ret;

            try {
                ret = nextFn();
            } catch(err) {
                // finished with failure, reject the promise
                reject(err);
                return;
            }

            if (ret.done) {
                // finished with success, resolve the promise
                resolve(ret.value);
                return;
            }

            // not finished, chain off the yielded promise and `step` again
            Promise.resolve(ret.value).then(function(value) {
                step(function() { return gen.next(value); });
            }, function(err) {
                step(function() { return gen.throw(err); });
            });
        }

        // Start it up
        step(function() {
            return gen.next(undefined); 
        });
    });
};
