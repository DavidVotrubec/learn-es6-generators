"use strict";

/**
 * Generator is a pauseable function
 * so let's show some really basic example of pausing and resuming
 */

function* myGenerator() {
    yield 1;
    yield 2;
    yield 3;
}

// create instance of an iterator
const it = myGenerator();

// Then iterate
console.log(it.next()); // outputs {value: 1, done: false}
console.log(it.next()); // outputs {value: 2, done: false}
console.log(it.next()); // outputs {value: 3, done: false}
console.log(it.next()); // outputs {value: undefined, done: true}