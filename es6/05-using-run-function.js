"use strict";

const run = require('./03-run-function');
const thunkify = require('./thunkify');

const fs = require('fs');
const readFile = thunkify(fs.readFile);
// The alternative solution to thunkifying is the 'mz' npm package https://www.npmjs.com/package/mz
// It modernizes nodejs API by wrapping its core functions into promises
// making them thenable
 
run(function* () {
  try {
    const fileContent = yield readFile('./es6/blabla.json');
    console.log(fileContent); // binary array, but we do not mind :)
  }
  catch (er) {
    console.error(er);
  }
})