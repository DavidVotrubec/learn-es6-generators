"use strict";

require("babel-core/register");
require("babel-polyfill");
var fetch = require('node-fetch');

// Based on example from Jake Archibald
// See https://jakearchibald.com/2015/thats-so-fetch/
// but added require()
(async() => {
  try {
    // url to randomly generated json
    const url = 'http://www.json-generator.com/api/json/get/cozIoacyaa?indent=2';

    // fetch is a new API, better than XMLHttpRequest
    // it returns a promise, so it can be awaited
    var response = await fetch(url);
    var data = await response.json();
    console.log('data arrived', data);
  } catch (err) {
    console.log("Booo", err);
  }
})();