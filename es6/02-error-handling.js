// Example #1
// Error is thrown into the generator
// and caught outside of it
function *foo() { }

const it1 = foo();
try {
    it1.throw( "Oops!" );
}
catch (err) {
    console.log( "Error was caught: " + err ); // Error: Oops!
}

// Example #2
// Error is thrown inside the generator
// but caught outside of it
function *errorFoo(){
    throw new Error('error was thrown inside the generator');
}

const it2 = errorFoo();
try {
    it2.next();
}
catch(err) {
    console.log('Something went wrong inside our generator', err);
}

// Example #3
// Error is thrown inside the generator
// but is never caught - it will end up as unhandled exception
const it3 = errorFoo();
it3.next();