"use strict";

// Example of resuming yielded generator and passing in a new variable

function *generator() {
    console.log('I have started');

    // It should be valid to just 'yield', but for some reason 'regenerator:all' does not like it
    yield undefined; // we yield nothing, so the returned value is undefined

    var a = yield "some value";
    console.log('a is now ', a); // 'a' will contain value passed into generator when it was resumed
}

(function(){
    var iterator = generator();
    var firstValue = iterator.next(); 
    // outputs {done: false, value: undefined}
    console.log('firstValue', firstValue); 

    var secondValue = iterator.next();
    // outputs {done: false, value: "some value"}
    console.log('secondValue', secondValue); 

    // resumes the generator, 'a' will now get value from the yield expression
    // and outputs it to console
    var thirdValue = iterator.next('bbb');
    // outputs {done: true, value: undefined}
    console.log('thirdValue', thirdValue);
})();