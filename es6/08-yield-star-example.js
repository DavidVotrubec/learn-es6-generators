"use strict";

var spawn = require('./04-spawn-function');

// Example of yeilding to another generator

function* caller() {
    yield 1;
    yield 2;

    // no need to manually call .next() etc
    const a = yield* callee();
    console.log(`variable a from callee() is ${a}`);

    yield 5;
}

function* callee() {
    yield 3;
    yield 4;
    return 'A from Callee';
}

// Each generator instance is also an iterator, so we can run for...of loop
for (var v of caller()){
    console.log(v);
}

// Outputs this into console.log
// 1
// 2
// 3
// 4
// variable a from callee() is A from Callee
// 5


spawn(caller);
// Outputs this into console.log
// variable a from callee() is A from Callee